# ANSIBLE的简单应用

[TOC]

### 一. `ansible`的配置，安装与测试

#### 1.1 `ansible`的安装

```python
# 著需要在服务器中进行安装
yum install ansible -y
```

#### 1.2 ansible的配置测试

> 修改主机清单
>
> 主机清单中添加要批量进行操作的主机IP
>
> 使用ansible的ping模块小试牛刀

<video id="video" controls="" preload="none">
    <source id="mp4" src="./ansible的简单应用.assets/ansible安装.mkv" type="video/mp4">
</video>

### 二. 定义主机与组规则

> :information_desk_person: 大部分配置在`/etc/ansible/hosts`中都有示例

#### 2.1 常见主机清单展示

> :information_desk_person:清单配置的越好，将来ansible-playbook使用的时候就越方便

```python
mail.example.com
192.168.1.21：2135
[webservers]
foo.example.com
bar.example.com
192.168.1.22
[dbservers]
one.example.com
two.example.com
three.example.com
192.168.1.23

# 可以使用别名
jumper ansible_ssh_port=22 ansible_ssh_host=192.168.1.50

"""
jumper为定义的一个别名，ansible_ssh_port为主机SSH服务端口，ansible_ssh_host为目标主机，更多保留主机变量如下：
·ansible_ssh_host，连接目标主机的地址。
·ansible_ssh_port，连接目标主机SSH端口，端口22无需指定。
·ansible_ssh_user，连接目标主机默认用户。
·ansible_ssh_pass，连接目标主机默认用户密码。
·ansible_connection，目标主机连接类型，可以是local、ssh或paramiko。
·ansible_ssh_private_key_file连接目标主机的ssh私钥。
·ansible_*_interpreter，指定采用非Python的其他脚本语言，如Ruby、Perl或其他类似ansible_python_interpreter解释器。
"""


# 支持正则
[webservers]
www[01：50].example.com
[databases]
db-[a：f].example.com
```

#### 2.2  定义变量

```python
"""
主机可以指定变量，以便后面供Playbooks配置使用，比如定义主机hosts1及hosts2上Apache参数http_port及maxRequestsPerChild，目的是让两台主机产生Apache配置文件httpd.conf差异化，定义格式如下：
"""

[atlanta]
host1 http_port=80 maxRequestsPerChild=808
host2 http_port=303 maxRequestsPerChild=909
```

#### 2.3 定义组变量

````python
"""
组变量的作用域是覆盖组所有成员，通过定义一个新块，块名由组名+“：vars”组成，定义格式如下：
"""

[atlanta]
host1
host2
[atlanta：vars]
ntp_server=ntp.atlanta.example.com
proxy=proxy.atlanta.example.com


"""
同时Ansible支持组嵌套组，通过定义一个新块，块名由组名+“：children”组成，格式如下：
"""
[atlanta]
host1
host2
[raleigh]
host2
host3
[southeast：children]
atlanta
raleigh
[southeast：vars]
some_server=foo.southeast.example.com
halon_system_timeout=30
self_destruct_countdown=60
escape_pods=2
[usa：children]
southeast
northeast
southwest

"""
southeast提示　嵌套组只能使用在/usr/bin/ansible-playbook中，在/usr/bin/ansible中不起作用。
"""
````

### 三. ansible命令的使用

```python
# 格式
ansible <pattern_goes_here（操作目标）> -m <module_name（模块名）> -a <module_args（模块参数）>

ansible webservers -m service -a "name=httpd state=restarted"
```

<font style="color:white;">其中默认的模块名为command，即“-m command”可省略</font>

![1637459730396](ANSIBLE的简单应用.assets/1637459730396.png)



### 四. ansible的常用模块以及api

Cloud（云计算）、Commands（命令行）、Database（数据库）、Files（文件管理）、Internal（内置功能）、Inventory（资产管理）、Messaging（消息队列）、Monitoring（监控管理）、Net Infrastructure（网络基础服务）、Network（网络管理）、Notification（通知管理）、Packaging（包管理）、Source Control（版本控制）、System（系统服务）、Utilities（公共服务）、Web Infrastructure（Web基础服务）

[ansible模块](https://docs.ansible.com/ansible/2.9/modules/modules_by_category.html#modules-by-category)

> :information_desk_person: 上千个模块别都学，用啥学啥，常用的也不多

#### 4.1 常见模块举例

##### 4.1.1 远程命令模块

- `命令模块`(默认)

  ```python
  ansible 47.108.148.217 -a hostname
  ```

- `script模块`（相当于scp+shell）

  ```python
  ansible 47.108.148.217 -m script -a "say_hello.sh"
  ```

- `shell`(万能模块,只能执行目标主机本地的脚本，有模块不会就写成shell脚本，调用shell模块，但是ansible并不推荐这样做，不方便调试，且容易出错)

  ```python
  ansible 47.108.148.217 -m shell -a "/root/say_nihao.sh"
  ```

> :information_desk_person:就不逐一举例了，多使用熟悉常用模块就行了，值得提醒的是ansible的监控模块中是支持zabbix的。

### 五. `playbook`的介绍

> :information_desk_person:`​playbook`是一个不同于使用`Ansible`命令行执行方式的模式，其功能更强大灵活。简单来说，`playbook`是一个非常简单的配置管理和多主机部署系统，不同于任何已经存在的模式，可作为一个适合部署复杂应用程序的基础。`playbook`可以定制配置，可以按指定的操作步骤有序执行，支持同步及异步方式。官方提供了大量的例子，可以在https://github.com/ansible/ansible-examples找到。`playbook`是通过`YAML`格式来进行描述定义的，可以实现多台主机应用的部署。

**yaml文件中主要要说明目标主机，以及要执行的任务**

```yaml
- hosts: all
  gather_facts: false 
  tasks:
    - name: 01-安装zabbix-agent
      command: rpm -ivh https://mirrors.tuna.tsinghua.edu.cn/zabbix/zabbix/3.2/rhel/7/x86_64/zabbix-agent-3.2.11-1.el7.x86_64.rpm
    - name: 02-备份客户端配置文件
      command: cp /etc/zabbix/zabbix_agentd.conf /etc/zabbix/zabbix_agentd.conf.bak 
    - name: 03-复制修改后的客户端文件给zabbix-agent
      copy: src=./zabbix_agentd.conf dest=/etc/zabbix/
    - name: 04-修改配置文件
      command: sed -i "s/Server=192.168.72.50/Server=192.168.72.132/" /etc/zabbix/zabbix_agentd.conf
    - name: 05-修改配置文件2
      command: sed -i "s/ServerActive=193.168.72.50/ServerActive=192.168.72.132/" /etc/zabbix/zabbix_agentd.conf
    - name: 06-重启zabbix-agent服务
      service: name=zabbix-agent state=restarted
```

> :information_desk_person:
>
> 运行
>
> - 检查语法格式:**ansible-playbook --syntax-check  /etc/ansible/main.yaml**
> - **模拟运行:ansible-playbook -C /etc/ansible/main.yaml****
> - 真正运行：**ansible-palybook /etc/ansible/main.yaml**
>
> 免交户分发公钥
>
> - **sshpass -p123456 ssh-copy-id -i /root/.ssh/id_dsa.pub root@172.16.1.31 -p 52113 "-o StrictHostKeyChecking=no"**
>
> 查看yaml文件内容的技巧
>
> - **cat -A**
> - **set list**

<p style="color:red">========================以下为高阶内容==========================</p>
#### 5.1 定义主机与用户

在`playbook`执行时，可以为主机或组定义变量，比如指定远程登录用户。以下为`webservers`组定义的相关变量，变量的作用域只限于`webservers`组下的主机

```yaml
 -hosts： webservers
  vars：
    worker_processes： 4
    num_cpus： 4
    max_open_file： 65506
    root： /data
  remote_user： root
```

> 扩展：主机变量定义的三种格式
>
> ```python
> 方式一：直接在剧本文件编写  
> vars:
>    oldboy01: data01
>    oldboy02: data02
>       
> 方式二：在命令行中进行指定
> ansible-playbook --extra-vars=oldboy01=data01
> 
> 方式三：在主机清单文件编写
> [oldboy]
> oldboy01=data01
> oldboy02=data02
> 
> 三种变量设置方式都配置了,三种方式的优先级???
> 最优先: 命令行变量设置
> 次优先: 剧本中变量设置
> 最后:   主机清单变量设置
> ```

#### 5.2 任务

> 所有定义的任务列表（tasks list），playbook将按定义的配置文件自上而下的顺序执行，定义的主机都将得到相同的任务，但执行的返回结果不一定保持一致，取决于主机的环境及程序包状态。建议每个任务事件都要定义一个name标签，好处是增强可读性，也便于观察结果输出时了解运行的位置，默认使用action（具体的执行动作）来替换name作为输出。下面是一个简单的任务定义示例：
> tasks：
>
>   - name： make sure nginx is running
>     service： name=nginx state=running

#### 5.3 使用注册信息查看任务执行

```python
- hosts: 192.168.72.132
  tasks:
    - name: check server port
      shell: netstat -nltup|grep 22
      register: get_server_port
    - name: display port info
      debug: msg={{ get_server_port.stdout_lines }}
```

#### 5.4 剧本内使用判断信息

```python
- hosts: 192.168.72.132
  remote_user: root
  tasks:
    - name: check zabbix-agent
      shell: netstat -nltup|grep 22
      when: (ansible_hostname == "nfs")
      #register: get_status
```

#### 5.5 剧本中使用循环信息

````python
- hosts: 192.168.72.132
  remote_user: root
  tasks:
    - name: add user
      user: name={{ item.name }} groups={{ item.groups }} state=present
      with_items:
        -  { name: 'guoshao', groups: 'root'}
        -  { name: 'test', groups: 'test'}

````

#### 5.6 剧本中忽略标签

```python
- hosts: 192.168.72.132
  remote_user: root
  tasks:
    - name: add user
      user: name={{ item.name }} groups={{ item.groups }} state=present
      ignore_errors: yes
      with_items:
        -  { name: 'guoshao', groups: 'root'}
        -  { name: 'test', groups: 'test'}

        
        
"""输出示例（发生错误时，忽略错误）"""
ok: [192.168.72.132] => (item={u'name': u'guoshao', u'groups': u'root'})
failed: [192.168.72.132] (item={u'name': u'test', u'groups': u'test'}) => {"ansible_loop_var": "item", "changed": false, "item": {"groups": "test", "name": "test"}, "msg": "Group test does not exist"}
...ignoring

PLAY RECAP ************************************************************************************************************
192.168.72.132             : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=1 
```

#### 5.7 剧本中设置标签功能

```python
"""只有一步执行错误时可以选择执行哪个标签"""
- hosts: 192.168.72.132
  remote_user: root
  tasks:
    - name: add user
      user: name={{ item.name }} groups={{ item.groups }} state=present
      ignore_errors: yes
      with_items:
        -  { name: 'guoshao', groups: 'root'}
        -  { name: 'test', groups: 'test'}
      tags: t1
    - name: check status
      shell: ps -ef|grep zabbix
      tags: t2 
        
        
        
# 执行
	指定执行哪个标签任务： ansible-playbook --tags=t2 test05.yml 
	跳过指定标签任务：     ansible-playbook --skip-tags=t2 test05.yml 		

```

#### 5.8 剧本中设置触发功能

```python
- hosts: 192.168.72.132
  remote_user: root
  tasks:
    - name: 01 install ruanjian
      yum: name=tree state=present
      notify: test
  handlers:
    - name: test
      shell: rpm -qa tree

```

#### 5.9 将多个剧本进行整合

```python
方式一：include_tasks: f1.yml
    - hosts: all
      remote_user: root
      tasks:
        - include_tasks: f1.yml
        - include_tasks: f2.yml

方式二：include: f1.yml
    - include：f1.yml	
    - include：f2.yml

方式三：- import_playbook:
[root@m01 ansible-playbook]# cat main.yml 
    - import_playbook: base.yml     
    - import_playbook: rsync.yml    
    - import_playbook: nfs.yml      
	- import_playbook: oxxx.yml
    - import_playbook: rsync.yml
    - import_playbook: nfs.yml
        
        
"""示例"""
#- include: a.yaml
#- include: b.yaml
- import_playbook: a.yaml
- import_playbook: b.yaml
```

#### 5.10 `playbook`的角色

> :information_desk_person:当我们写一个非常大的playbook时，想要复用些功能显得有些吃力，还好Ansible支持写playbook时拆分成多个文件，通过包含（include）的形式进行引用，我们可以根据多种维度进行“封装”，比如定义变量、任务、处理程序等。
> 角色建立在包含文件之上，抽象后更加清晰、可复用。运维人员可以更专注于整体，只有在需要时才关注具体细节.Ansible官方在GitHub上提供了大量的示例供大家参考借鉴，访问地址https://github.com/ansible/ansible-examples即可获相应的学习资料。

现在我们已经了解了变量、任务、处理程序的定义，有什么方法更好地进行组织或抽象，让其复用性更强、功能更具模块化？答案就是角色。角色是Ansible定制好的一种标准规范，以不同级别目录层次及文件对角色、变量、任务、处理程序等进行拆分，为后续功能扩展、可维护性打下基础。

```python
../nginx
├── group_vars        #### 这里描述的是全局变量，roles里定义变量的优先级高于在这定义的
│   ├── all
│   └── webserver
├── hosts             #### 这里是playbook将在哪些主机上运行，与/etc/ansible/hosts同理
├── roles             #### 所有的角色都在这个文件夹
│   ├── common
│   │   ├── files     ### 这里是要分享的文件
│   │   ├── handlers  ### 这里是触发器
│   │   │   └── main.yaml  
│   │   ├── tasks     ### 这里是任务
│   │   │   └── main.yaml
│   │   ├── templates ### 这里是模板，支持jinja2模板语法
│   │   │   └── ntp.conf.j2
│   │   └── vars      ### 这里是这个角色私有的能够使用的变量
│   │       └── main.yaml
│   └── web
│       ├── files
│       ├── handlers
│       │   └── main.yaml
│       ├── tasks
│       │   └── main.yaml
│       ├── templates
│       │   └── nginx2.conf.j2
│       └── vars
│           └── main.yaml
└── site.yaml

14 directories, 13 files

```

#### 5.11 `jinja2`过滤

[更多过滤方法](https://jinja.palletsprojects.com/en/3.0.x/templates/#builtin-filters)

```python
- hosts: 192.168.72.51
  vars:
    filename: /etc/profile
  tasks:
    - name: "测试jinja2过滤"
      shell: echo {{ filename|basename }} >> /tmp/testshell
{{变量名|过滤方法}}
```

