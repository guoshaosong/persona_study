## 1.使用ratelimt设置接口请求频率限制

![1634003388403](user.assets/1634003388403.png)

```
pip3 install django-ratelimit



rate = lambda group, request: settings.RATELIMIT_LOGIN if request.session.get('islogin') else settings.RATELIMIT_NOLOGIN


key = lambda group, request: request.META['HTTP_X_REAL_IP'] if request.META.get('HTTP_X_REAL_IP') else request.META[
    'REMOTE_ADDR']
```

## 2.wraps函数的使用

Python 装饰器中的@wraps的作用：
    装饰器的作用:    在不改变原有功能代码的基础上,添加额外的功能,如用户验证等
    @wraps(view_func)的作用:     不改变使用装饰器原有函数的结构(如__name__, __doc__)
    不使用wraps可能出现的ERROR:   view_func...endpoint...map...

