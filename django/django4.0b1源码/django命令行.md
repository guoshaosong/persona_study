```python
D:\django-main\django\core\management\__init__.py
    
def find_commands(management_dir):
    """
    Given a path to a management directory, return a list of all the command
    names that are available.
    """
    command_dir = os.path.join(management_dir, 'commands')
    return [name for _, name, is_pkg in pkgutil.iter_modules([command_dir])
            if not is_pkg and not name.startswith('_')]
```

```python
导入一个包，然后使用__path__可以该模块的路径
import test
print(test.__path__)

使用os模块可以切割文件与文件后缀
name, ext = os.path.splitext(file)


```

**pkgutil**

````python
pkgutil是Python自带的用于包管理相关操作的库，pkgutil能根据包名找到包里面的数据文件，然后读取为bytes型的数据。如果数据文件内容是字符串，那么直接decode()以后就是正文内容了。	

为什么pkgutil读取的数据文件是bytes型的内容而不直接是字符串类型?
这是因为并不是所有数据文件都是字符串，如果某些数据文件是二进制文件或者图片，那么以字符串方式打开就会导致报错。所以为了通用，pkgutil会以bytes型方式读入数据，这相当于open函数的“rb”读取方式。


import pkgutil
 
# 用法1读文件
data_bytes = pkgutil.get_data(__package__, 'data.txt')
data_str = data_bytes.decode()
print(data_str)

# 用法2获取所有模块
import test

print(test.__path__)
print(test.__name__)
for _, name, is_pkg in pkgutil.iter_modules(test.__path__, test.__name__ + "."):
    print("{0} name: {1:12}, is_sub_package: {2}".format(_, name, is_pkg))
print([name for _, name, is_pkg in pkgutil.iter_modules(test.__path__, "这是找的的模块") if not is_pkg and not name.startswith('_')])

````

**argparse**

````python
# 该模块主要用于参数解析
# 基本结构很简单，就只有四步：
ArgumentParser.add_argument(name or flags...[, action][, nargs][, const][, default][, type][, choices][, required][, help][, metavar][, dest])

# 引入包
# 创建参数对象
# 添加参数
# 解析对象
import argparse

parser = argparse.ArgumentParser(description='parser demo')
parser.add_argument('a', help='argument for a', type=int)
parser.add_argument('b', help='argument for b', type=int)
args = parser.parse_args()
multi = args.a * args.b
print(multi)

# 以-或开头的参数–通常被认为是可选的，即可以不写的参数。
"""
单破折号与双破折号的区别：单破折号表示后面跟一个字符，双破折号表示后面跟多个字符。
可以用单引号或者双引号为参数赋值，具体情况如下图所示
"""
parser = argparse.ArgumentParser(description="这是一个统计模块")
parser.add_argument('n',  default='Lucy')
parser.add_argument('s', default='male')
parser.add_argument('-tel', '--tel', default='male', help="这是一个可选的选项")
args = parser.parse_args()
print(args)
name = args.n
sex = args.s
print('Hello {}  {}'.format(name, sex))
````

