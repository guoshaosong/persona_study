# 系统批量运维管理器paramiko详解

[TOC]

#### 一.  简介

paramiko是基于Python实现的SSH2远程安全连接，支持认证及密钥方式。可以实现远程命令执行、文件传输、中间SSH代理等功能

#### 二. paramiko的核心组件

##### 2.1 sshclient类

```python
#!/usr/bin/env python
# -- coding: utf-8 --
# @Author : 宇智波---郭少
# @File : paramiko_demo.py
import paramiko

hostname = "192.168.72.52"
username = "root"
password = "111111"
# 发送paramiko日志到指定文件夹
paramiko.util.log_to_file('syslogin.log')
# 创建一个ssh客户端的client对象
ssh = paramiko.SSHClient()
# 允许链接不在konw_hosts文件中的主机，autoaddpolicy自动添加免密，prjectpolicy(默认)，warningproject(会有警告)
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
# 获取客户端host_keys，默认~/.ssh/known_hosts，,免密登录
# ssh.load_system_host_keys()
# 非默认路需要指定
# privatekey = os.path.expanduser（'/home/key/id_rsa'）  #定义私钥存放路径
# key = paramiko.RSAKey.from_private_key_file（privatekey）  #创建私钥对象key
# 创建ssh链接
ssh.connect(hostname=hostname, username=username, password=password)
# 远程调用命令执行
stdin, stdout, stderr = ssh.exec_command('ifconfig')
# 打印命令执行结果，得到Python列表形式，可以使用stdout.readlines（）
print(stdout.readlines())
# print(stderr.read())
ssh.close()

```

##### 2.2 sftpclient类

```python

"""paramiko的sftpclient类"""
import paramiko

hostname = "192.168.72.52"
username = "root"
password = "111111"
port = 22
try:
    # 创建一个已连接的sftp客户端通道
    t = paramiko.Transport((hostname, port))
    t.connect(username=username, password=password)
    sftp = paramiko.SFTPClient.from_transport(t)
    # 上传文件
    sftp.put('/root/ENV/smartwarehouse/smartwarehouse/utils/models.py', '/root/models.py')
    sftp.get('/root/models.py', '/root/models.py')
    # 创建目录
    sftp.mkdir('/root/guoshaosong', 755)
    sftp.mkdir('/root/userdir', 755)
    # 删除文件
    sftp.rmdir('/root/userdir')
    # 文件重命名
    sftp.rename('/root/models.py', '/root/models.bak.py')
    # 打印文件信息
    print(sftp.stat('/root/models.bak.py'))
    # 打印目录列表
    print(sftp.listdir('/root'))
    t.close()
except Exception as e:
    print(e)

```

##### 2.3 paramiko应用实例（利用堡垒机执行远程命令）

> 原理：原理是SSHClient.connect到堡垒机后开启一个新的SSH会话（session），通过新的会话运行“ssh user@IP”去实现远程执行命令的操作











```python
import paramiko
import os
import select
import sys
import tty
import termios

'''
实现一个xshell登录系统的效果，登录到系统就不断输入命令同时返回结果
支持自动补全，直接调用服务器终端

'''
# 建立一个socket
trans = paramiko.Transport(('192.168.2.129', 22))
# 启动一个客户端
trans.start_client()

# 如果使用rsa密钥登录的话
'''
default_key_file = os.path.join(os.environ['HOME'], '.ssh', 'id_rsa')
prikey = paramiko.RSAKey.from_private_key_file(default_key_file)
trans.auth_publickey(username='super', key=prikey)
'''
# 如果使用用户名和密码登录
trans.auth_password(username='super', password='super')
# 打开一个通道
channel = trans.open_session()
# 获取终端
channel.get_pty()
# 激活终端，这样就可以登录到终端了，就和我们用类似于xshell登录系统一样
channel.invoke_shell()

# 获取原操作终端属性
oldtty = termios.tcgetattr(sys.stdin)
try:
    # 将现在的操作终端属性设置为服务器上的原生终端属性,可以支持tab了
    tty.setraw(sys.stdin)
    channel.settimeout(0)

    while True:
        readlist, writelist, errlist = select.select([channel, sys.stdin,], [], [])
        # 如果是用户输入命令了,sys.stdin发生变化
        if sys.stdin in readlist:
            # 获取输入的内容，输入一个字符发送1个字符
            input_cmd = sys.stdin.read(1)
            # 将命令发送给服务器
            channel.sendall(input_cmd)

        # 服务器返回了结果,channel通道接受到结果,发生变化 select感知到
        if channel in readlist:
            # 获取结果
            result = channel.recv(1024)
            # 断开连接后退出
            if len(result) == 0:
                print("\r\n**** EOF **** \r\n")
                break
            # 输出到屏幕
            sys.stdout.write(result.decode())
            sys.stdout.flush()
finally:
    # 执行完后将现在的终端属性恢复为原操作终端属性
    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, oldtty)

# 关闭通道
channel.close()
# 关闭链接
trans.close()
```



