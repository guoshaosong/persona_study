

### and的优先级高于or

```mysql
select prod_name,prod_price from products where vend_id=1002 or vend_id=1003 and prod_price >= 10;
#默认会有优先执行and运算，如果想要先执行or运算，应该加上小括号。
select prod_name,prod_price from products where (vend_id=1002 or vend_id=1003) and prod_price >= 10;
```

### 通配符

```mysql
%匹配任意字符
_匹配一个字符
```

### 正则

```` shell
select prod_name from products where prod_name regexp '1000' order by prod_name;
#使用or匹配
select prod_name from products where prod_name regexp '1000|2000' order by prod_name;
#匹配几个字符之一
select prod_name from products where prod_name regexp '[123] ton' order by
prod_name;
#匹配范围
[1-9]
#匹配特殊字符
//.
````

### concate将值连起来构成单个值

```mysql
MYSQL中CONCAT()用于连接两个或多个字符串：
1、concat(str1, str1, …)返回：“str1str2”
2、 CONCAT_WS(separator,str1,str2,…)：用指定的字符连接两个字符创：
例如：concat_ws("->", “aa”, “bb”)，则返回：“aa->bb”
```

### 数据处理函数

```
去掉右边空格rtrim()
将文本转换成大写upper()
日期处理函数
select cust_id,order_num from orders where DATE(order_date)='2015-09-01';
select cust_id,order_num from orders where Year(order_date)=2015 and Month(order_date)=9;
```

### mysql的各种连接

![](https://gitee.com/guoshaosong/chuangtu/raw/master/images/202203142050767.png)

### 组合查询

```mysql
MariaDB [guoshao]> select vend_id,prod_id,prod_price from products where prod_price<=5 union select vend_id,prod_id,prod_price from products where vend_id in (1002,1002);
+---------+---------+------------+
| vend_id | prod_id | prod_price |
+---------+---------+------------+
|    1003 | FC      |       2.50 |
|    1002 | FU1     |       3.42 |
|    1003 | SLING   |       4.49 |
|    1003 | TNT1    |       2.50 |
|    1002 | OL1     |       8.99 |
+---------+---------+------------+
5 rows in set (0.00 sec)


#与其相同的查询
select vind_id,prod_id,prod_price from products where prod_price <=5 or vend_id in (1001,1002);

#使用union可能比where更加复杂，但是对于更加复杂的顾虑条件，使用union可能会让处理更加简单。
#union会默认取消重复列，想要返回所有列使用union all
```

### 插入，更新与删除数据

```mariadb
MariaDB [guoshao]> INSERT INTO customers(cust_id, cust_name, cust_address, cust_city, cust_state, cust_zip, cust_country, cust_contact, cust_email) VALUES(10012, 'Coyote Inc.', '200 Maple Lane', 'Detroit', 'MI', '44444', 'USA', 'Y Lee', 'ylee@coyote.com');
Query OK, 1 row affected (0.00 sec)

MariaDB [guoshao]> update customers set cust_name = "guoshao" where cust_id=10012;
Query OK, 1 row affected (0.00 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [guoshao]> select cust_id,cust_name from customers where cust_id=10012;
+---------+-----------+
| cust_id | cust_name |
+---------+-----------+
|   10012 | guoshao   |
+---------+-----------+
1 row in set (0.00 sec)


MariaDB [guoshao]> delete from customers where cust_id=10012;
Query OK, 1 row affected (0.00 sec)

MariaDB [guoshao]> select cust_id,cust_name from customers where cust_id=10012;
Empty set (0.00 sec)


```

### 更新，删除与重命名表

```mariadb
MariaDB [guoshao]> create table guoshao_test(
    -> name varchar(5) not null, 
    -> grade int(3) not null);
Query OK, 0 rows affected (0.00 sec)

MariaDB [guoshao]> alter table guoshao_test add age int(3);
Query OK, 0 rows affected (0.01 sec)               
Records: 0  Duplicates: 0  Warnings: 0

MariaDB [guoshao]> alter table guoshao_test drop column age;
Query OK, 0 rows affected (0.00 sec)               
Records: 0  Duplicates: 0  Warnings: 0

MariaDB [guoshao]> drop table guoshao_test;
Query OK, 0 rows affected (0.01 sec)

MariaDB [guoshao]> rename table guoshao_test to guoshaosong_test;
Query OK, 0 rows affected (0.00 sec)

#也可以用来添加外键
alter table guoshao_test add constraint fk foreign key(order num) references oders(order num);
```

### 视图

- 重用sql语句
- 简化复杂的sql操作
- 使用表的组成部分而不是整个表
- 保护数据，可以给用户授予表的特定部分的访问权限而不是整个表的访问权限
- 更改数据格式和表示

```mariadb
MariaDB [guoshao]> create view gss as select * from customers  where cust_id=10001;
Query OK, 0 rows affected (0.00 sec)


MariaDB [guoshao]> select * from gss;
+---------+-------------+----------------+-----------+------------+----------+--------------+--------------+-----------------+
| cust_id | cust_name   | cust_address   | cust_city | cust_state | cust_zip | cust_country | cust_contact | cust_email      |
+---------+-------------+----------------+-----------+------------+----------+--------------+--------------+-----------------+
|   10001 | Coyote Inc. | 200 Maple Lane | Detroit   | MI         | 44444    | USA          | Y Lee        | ylee@coyote.com |
+---------+-------------+----------------+-----------+------------+----------+--------------+--------------+-----------------+
1 row in set (0.00 sec)

MariaDB [guoshao]> drop view gss;
Query OK, 0 rows affected (0.00 sec)

```

### ifnull函数

![image-20220315084755536](https://gitee.com/guoshaosong/chuangtu/raw/master/images/202203150847673.png)

```mariadb
#查找第n高的工资
CREATE FUNCTION getNthHighestSalary(N INT) RETURNS INT
BEGIN
 set N = N - 1;
 RETURN (
      # Write your MySQL query statement below.
      select ifnull((
          select distinct Salary 
          from Employee
          order by Salary desc limit N,1),null) as getNthHighestSalary
  );
END
```

### datediff函数

![image-20220315095733762](https://gitee.com/guoshaosong/chuangtu/raw/master/images/202203150957867.png)

````mariadb
mysql> SELECT DATEDIFF('2017-08-17','2017-08-08');
+-------------------------------------+
| DATEDIFF('2017-08-17','2017-08-08') |
+-------------------------------------+
|                                   9 |
+-------------------------------------+
1 row in set


表： Weather

+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| id            | int     |
| recordDate    | date    |
| temperature   | int     |
+---------------+---------+
id 是这个表的主键
该表包含特定日期的温度信息
 

编写一个 SQL 查询，来查找与之前（昨天的）日期相比温度更高的所有日期的 id 


# Write your MySQL query statement below
SELECT
    weather.id AS 'Id'
FROM
    weather
        JOIN
    weather w ON DATEDIFF(weather.date, w.date) = 1
        AND weather.Temperature > w.Temperature
;
````

### if函数

![image-20220316104801068](https://gitee.com/guoshaosong/chuangtu/raw/master/images/202203161048799.png)



```
SELECT 
IF(age>=25,'25岁及以上','25岁以下') AS age_cut, 
COUNT(device_id) AS number 
FROM user_profile 
GROUP BY age_cut
```

### case函数

```
CASE 测试表达式
WHEN 简单表达式1 THEN 结果表达式1
WHEN 简单表达式2 THEN 结果表达式2 …
WHEN 简单表达式n THEN 结果表达式n
[ ELSE 结果表达式n+1 ]
END


SELECT CASE WHEN age < 25 OR age IS NULL THEN '25岁以下' 
            WHEN age >= 25 THEN '25岁及以上'
            END age_cut,COUNT(*)number
FROM user_profile
GROUP BY age_cut
```

### 日期函数

```
day(date)
month(date)
year(date)
date_format(date, "%Y-%m")="202108"
group by date
```

### 文本函数

![图片说明](https://gitee.com/guoshaosong/chuangtu/raw/master/images/202203191232650.png)

```mariadb
SELECT SUBSTRING_INDEX(profile,",",-1) gender,COUNT(*) number
FROM user_submit 
GROUP BY gender;
```



```mariadb
#为了判断一个值在某一列中是不是唯一的，我们可以使用 GROUP BY 和 COUNT。
#LETCODE 585

#CONCAT(s1,s2...sn)	字符串 s1,s2 等多个字符串合并为一个字符串 合并多个字符串
#SELECT CONCAT("SQL ", "Runoob ", "Gooogle ", "Facebook") AS ConcateString;



SELECT
    SUM(insurance.TIV_2016) AS TIV_2016
FROM
    insurance
WHERE
    insurance.TIV_2015 IN
    (
      SELECT
        TIV_2015
      FROM
        insurance
      GROUP BY TIV_2015
      HAVING COUNT(*) > 1
    )
    AND CONCAT(LAT, LON) IN
    (
      SELECT
        CONCAT(LAT, LON)
      FROM
        insurance
      GROUP BY LAT , LON
      HAVING COUNT(*) = 1
    )
;

```

![image-20220316144525252](https://gitee.com/guoshaosong/chuangtu/raw/master/images/202203161445400.png)

![image-20220316151007123](https://gitee.com/guoshaosong/chuangtu/raw/master/images/202203161510290.png)
