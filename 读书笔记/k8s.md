## k8s读书笔记

> 版权信息
> 书名：每天5分钟玩转Kubernetes
> 作者：CloudMan
> 出版日期：2018-04-01
> 出版社：清华大学出版社
> ISBN：9787302496670
> 本书由清华大学出版社有限公司授权京东电子版制作与发行



### 重要概念

#### Cluster

`Cluste`r是计算、存储和网络资源的集合，`Kubernetes`利用这些资源运行各种基于容器的应用。

#### Master

`Master`是`Cluster`的大脑，它的主要职责是调度，即决定将应用放在哪里运行。Master运行Linux操作系统，可以是物理机或者虚拟机。为了实现高可用，可以运行多个Master。

#### Node

`Node`的职责是运行容器应用。`Node`由`Master`管理，`Node`负责监控并汇报容器的状态，同时根据`Master`的要求管理容器的生命周期。`Node`运行在`Linux`操作系统上，可以是物理机或者是虚拟机。

#### Pod

`Pod`是`Kubernetes`的最小工作单元。每个`Pod`包含一个或多个容器。`Pod`中的容器会作为一个整体被`Master`调度到一个`Node`上运行。
`Kubernetes`引入Pod主要基于下面两个目的：
**（1）可管理性。**
有些容器天生就是需要紧密联系，一起工作。`Pod`提供了比容器更高层次的抽象，将它们封装到一个部署单元中。`Kubernetes`以`Pod`为最小单位进行调度、扩展、共享资源、管理生命周期。
**（2）通信和资源共享。**

`Pod`中的所有容器使用同一个网络`namespace`，即相同的`IP`地址和`Port`空间。它们可以直接用`localhost`通信。同样的，这些容器可以共享存储，当`Kubernetes`挂载`volume`到`Pod`，本质上是将`volume`挂载到`Pod`中的每一个容器。
Pods有两种使用方式：

- 运行单一容器。

  `one-container-per-Pod`是`Kubernetes`最常见的模型，这种情况下，只是将单个容器简单封装成Pod。即便是只有一个容器，`Kubernetes`管理的也是`Pod`而不是直接管理容器。

- 运行多个容器。

  问题在于：哪些容器应该放到一个`Pod`中？

  答案是：这些容器联系必须非常紧密，而且需要直接共享资源。

#### Controller

`Kubernetes`通常不会直接创建`Pod`，而是通过`Controller`来管理Pod的。`Controller`中定义了`Pod`的部署特性，比如有几个副本、在什么样的`Node`上运行等。为了满足不同的业务场景，`Kubernetes`提供了多种`Controller`，包括`Deployment`、`ReplicaSet、DaemonSet、StatefuleSet、Job`等，我们逐一讨论。
（1）`Deployment`是最常用的`Controller`，`Deployment`可以管理`Pod`的多个副本，并确保`Pod`按照期望的状态运行。
（2）`ReplicaSet`实现了`Pod`的多副本管理。使用`Deployment`时会自动创建`ReplicaSet`，也就是说`Deployment`是通过`ReplicaSe`来管理`Pod`的多个副本的，我们通常不需要直接使用`ReplicaSet`。
（3）`DaemonSet`用于每个`Node`最多只运行一个`Pod`副本的场景。正如其名称所揭示的，`DaemonSet`通常用于运行`daemon`。
（4）`StatefuleSet`能够保证`Pod`的每个副本在整个生命周期中名称是不变的，而其他`Controller`不提供这个功能。当某个Pod发生故障需要删除并重新启动时，Pod的名称会发生变化，同时`StatefuleSet`会保证副本按照固定的顺序启动、更新或者删除。
（5）`Job`用于运行结束就删除的应用，而其他`Controller`中的`Pod`通常是长期持续运行

#### Service

`Deployment`可以部署多个副本，每个`Pod`都有自己的`IP`，外界如何访问这些副本呢？
通过`Pod`的`IP`吗？
要知道`Pod`很可能会被频繁地销毁和重启，它们的`IP`会发生变化，用`IP`来访问不太现实。
答案是`Service`。
`Kubernetes Service`定义了外界访问一组特定`Pod`的方式。`Service`有自己的`IP`和端口，`Service`为`Pod`提供了负载均衡。
`Kubernetes`运行容器（Pod）与访问容器（Pod）这两项任务分别由`Controller`和`Service`执行。

#### Namespace

`Namespace`可以将一个物理的`Cluster`逻辑上划分成多个虚拟`Cluster`，每个`Cluster`就是一个`Namespace`。不同`Namespace`里的资源是完全隔离的。

**默认有2个namespace**

- default：创建资源时如果不指定，将被放到这个Namespace中。

- kube-system：Kubernetes自己创建的系统资源将放到这个Namespace中。

### kubernetes架构

Kubernetes Cluster由Master和Node组成，节点上运行着若干Kubernetes服务。

#### master节点

**主要有以下5个服务**

![image-20211225113648581](https://gitee.com/guoshaosong/chaungtu/raw/master/images/202112251136651.png)

- api-server:kubernetes cluster的前端接口
- sheduler：pod放在那个node节点上
- controller manager：管理cluster的各种资源
- etcd：保存cluster的配置及状态信息
- pod网络：pod之间要进行通信，flannel是一种解决方案

#### node节点

- kubelet：node节点的agent，当Scheduler确定在某个Node上运行Pod后，会将Pod的具体配置信息（image、volume等）发送给该节点的kubelet，kubelet根据这些信息创建和运行容器，并向Master报告运行状态。`该服务是没有以容器方式进行运行的服务`，可使用systemctl status kubelet进行常看。
- kube-proxy：service在逻辑上代表了后端的多个Pod，外界通过service访问Pod。service接收到的请求是如何转发到Pod的呢？这就是kube-proxy要完成的工作。
  每个Node都会运行kube-proxy服务，它负责将访问service的TCP/UPD数据流转发到后端的容器。如果有多个副本，kube-proxy会实现负载均衡。
- pod网路

#### 案例验证

**部署一个httpd应用**

```
kubectl run httpd-app --image=httpd --replicas=2
```

**内部流程**

![image-20211225141713381](https://gitee.com/guoshaosong/chaungtu/raw/master/images/202112251421455.png)

① kubectl发送部署请求到API Server。
② API Server通知Controller Manager创建一个deployment资源。
③ Scheduler执行调度任务，将两个副本Pod分发到k8s-node1和k8s-node2。
④ k8s-node1和k8s-node2上的kubectl在各自的节点上创建并运行Pod。
补充两点：
（1）应用的配置和当前状态信息保存在etcd中，执行kubectl get pod时API Server会从etcd中读取这些数据。
（2）flannel会为每个Pod都分配IP。因为没有创建service，所以目前kube-proxy还没参与进来

### 运行应用

> kubernetes通过controller来管理pod的生命周期，常用的有deployment

#### 运行deployment

```
kubectl run httpd-app --image=httpd --replicas=2
```

![](https://gitee.com/guoshaosong/chaungtu/raw/master/images/202112251445651.png)

（1）用户通过kubectl创建Deployment。
（2）Deployment创建ReplicaSet。
（3）ReplicaSet创建Pod。
对象的命名方式是“子对象的名字=父对象名字+随机字符串或数字”。

#### 命令vs配置文件

kubernete支持两种创建资源的方式

1. 使用kubectl命令进行创建。
2. 实用配置文件和kubectl apply创建，例如kubectl apply -f nginx.yml。（可重复部署，像代码一样进行管理）

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
spec:
  replicas: 2
  template:
    metadata:
      labels:
        app: web_server
    spec:
      containers:
      - name: nginx
        image: nginx:1
        ports:
        - containerPort: 80
        
        
#####参数说明#####
① apiVersion是当前配置格式的版本。
② kind是要创建的资源类型，这里是Deployment。
③ metadata是该资源的元数据，name是必需的元数据项。
④ spec部分是该Deployment的规格说明。
⑤ replicas指明副本数量，默认为1。
⑥ template定义Pod的模板，这是配置文件的重要部分。
⑦ metadata定义Pod的元数据，至少要定义一个label。label的key和value可以任意指定。
⑧ spec描述Pod的规格，此部分定义Pod中每一个容器的属性，name和image是必需的。

```

#### 用label控制Pod的位置

默认配置下，Scheduler会将Pod调度到所有可用的Node。不过有些情况我们希望将Pod部署到指定的Node，比如将有大量磁盘I/O的Pod部署到配置了SSD的Node；或者Pod需要GPU，需要运行在配置了GPU的节点上。
Kubernetes是通过label来实现这个功能的。label是key-value对，各种资源都可以设置label，灵活添加各种自定定义属性。

**添加node节点的标签**

```
创建label
kubectl label node k8s-node1 disktype=ssd
删除label
kubectl label node k8s-node1 disktype-
```

**查看所有node节点标签**

```
kubectl get node --show-labels
```

**在配置文件中进行使用**

![](https://gitee.com/guoshaosong/chaungtu/raw/master/images/202112251621890.png)

#### daemonset

> 每个node节点只会运行一个副本

DaemonSet的典型应用场景有：
（1）在集群的每个节点上运行存储Daemon，比如glusterd或ceph。
（2）在每个节点上运行日志收集Daemon，比如flunentd或logstash。
（3）在每个节点上运行监控Daemon，比如Prometheus Node Exporter或collectd。

#### job

> 容器按照持续运行的时间可分为两类：服务类容器和工作类容器。
> 服务类容器通常持续提供服务，需要一直运行，比如HTTP Server、Daemon等。工作类容器则是一次性任务，比如批处理程序，完成后容器就退出。
> Kubernetes的Deployment、ReplicaSet和DaemonSet都用于管理服务类容器；对于工作类容器，我们使用Job。

​	

```yaml
apiVersion: batch/v1
kind: Job
metadata:
  name: myjob
spec:
  completions: 6
  parallelism: 2
  template:
    metadata:
      name: myjob
    spec:
      containers:
      - name: hello
        image: busybox
        command: ["echo","hello k8s job!"]
      restartPolicy: Never



###
如果不希望失敗后重复进行尝试，可将restartPolicy设置为OnFailure
有时我们希望能同时运行多个Pod，提高Job的执行效率。这个可以通过parallelism设置
也可以设置运行成功多少次后结束，使用completeions
###
```

**查看輸出**

```
kubectl logs myjob-v9229
kubectl get job
```

![image-20211225170501229](https://gitee.com/guoshaosong/chaungtu/raw/master/images/202112251705295.png)

**并行运行**

![image-20211225172524574](https://gitee.com/guoshaosong/chaungtu/raw/master/images/202112251725640.png)

![image-20211225172554287](https://gitee.com/guoshaosong/chaungtu/raw/master/images/202112251725332.png)

**定时job**

> Linux中有cron程序定时执行任务，Kubernetes的CronJob提供了类似的功能，可以定时执行Job

```yaml
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: hello
spec:
  schedule: "*/1 * * * *"
  jobTemplate:
    spec:
      template:
        spec:
          containers:
          - name: hello
            image: busybox
            args:
            - /bin/sh
            - -c
            - date; echo Hello from the Kubernetes cluster
          restartPolicy: OnFailure

###説明###
有 5 个字段，以空格分隔。这些字段代表以下内容：
分钟（介于 0 到 59 之间）
小时（介于 0 到 23 之间）
一个月中的某天（介于 1 到 31 之间）
月份（介于 1 到 12 之间）
周几（介于 0 到 6 之间）

kubectl get cronjob 查看cronjob的状态
kubectl delete -f cronjob.yml 删除cronjob
```

![image-20211225194514672](https://gitee.com/guoshaosong/chaungtu/raw/master/images/202112251945738.png)

### 通过service访问pod

> pod是脆弱的，但是deployment（应用）应该是健壮的。
>
> 每个Pod都有自己的IP地址。当Controller用新Pod替代发生故障的Pod时，新Pod会分配到新的IP地址。这样就产生了一个问题：
> 如果一组Pod对外提供服务（比如HTTP），它们的IP很有可能发生变化，那么客户端如何找到并访问这个服务呢？
> Kubernetes给出的解决方案是Service。
>
> Kubernetes Service从逻辑上代表了一组Pod，具体是哪些Pod则是由label来挑选的。Service有自己的IP，而且这个IP是不变的。客户端只需要访问Service的IP，Kubernetes则负责建立和维护Service与Pod的映射关系。无论后端Pod如何变化，对客户端不会有任何影响，因为Service没有变。

#### 创建service

```
#先创建应用
kubectl create deployment nginx --imgae=nginx
#创建service
apiVersion: v1
kind: Service
metadata:
  name: nginxsvc
spec:
  type: NodePort
  ports:
    - port: 80
      nodePort: 30001
  selector:
    app: nginx
```

![](https://gitee.com/guoshaosong/chaungtu/raw/master/images/202112261127871.png)

> pod再怎么变化，但是service的ip是固定的，可以固定的是用service的ip地址访问服务。

#### cluster-ip

> CLUSTER-IP又是如何映射到Pod IP的呢？
> 答案是iptables。
>
> Cluster IP是一个虚拟IP，是由Kubernetes节点上的iptables规则管理的。
> 可以通过iptables-save命令打印出当前节点的iptables规则
>
> iptables将访问Service的流量转发到后端Pod，而且使用类似轮询的负载均衡策略。
> 另外，需要补充一点：Cluster的每一个节点都配置了相同的iptables规则，这样就确保了整个Cluster都能够通过Service的Cluster IP访问Service

#### dns访问service

![image-20211226113710292](https://gitee.com/guoshaosong/chaungtu/raw/master/images/202112261137345.png)

> kube-dns是一个DNS服务器。每当有新的Service被创建，kube-dns会添加该Service的DNS记录。Cluster中的Pod可以通过<SERVICE_NAME>.<NAMESPACE_NAME>访问Service。
>
> 如果要访问其他namespace中的Service，就必须带上namesapce了。kubectl get namespace查看已有的namespace

#### 外网访问service

> Kubernetes提供了多种类型的Service，默认是ClusterIP。
>
> （1）ClusterIP
> Service通过Cluster内部的IP对外提供服务，只有Cluster内的节点和Pod可访问，这是默认的Service类型，前面实验中的Service都是ClusterIP。
> （2）NodePort
> Service通过Cluster节点的静态端口对外提供服务。Cluster外部可以通过<NodeIP>:<NodePort>访问Service。Kubernetes会从30000～32767中分配一个可用的端口，每个节点都会监听此端口并将请求转发给Service，可通过nodeport指定端口，例子前面有。
> （3）LoadBalancer
> Service利用cloud provider特有的load balancer对外提供服务，cloud provider负责将load balancer的流量导向Service。目前支持的cloud provider有GCP、AWS、Azur等。

### 滚动升级

#### 升级

**1.部署2副本的应用，初始镜像为httpd:2.2.31，然后将其更新到httpd:2.2.32**

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: http-deployment
  labels:
    app: http
spec:
  replicas: 2
  selector:
    matchLabels:
      app: http
  template:
    metadata:
      labels:
        app: http
    spec:
      containers:
      - name: http
        image: httpd:2.2.31
        ports:
        - containerPort: 80
```

![image-20211226145551484](https://gitee.com/guoshaosong/chaungtu/raw/master/images/202112261455545.png)

**2.更改版本号**

![image-20211226145754058](https://gitee.com/guoshaosong/chaungtu/raw/master/images/202112261457111.png)

```
kubectl describe deployment http-deployment

每次替换的Pod数量是可以定制的。Kubernetes提供了两个参数maxSurge和maxUnavailable来精细控制Pod的替换数量
```

![image-20211226150246908](https://gitee.com/guoshaosong/chaungtu/raw/master/images/202112261502971.png)

#### 回滚

> kubectl apply每次更新应用时，Kubernetes都会记录下当前的配置，保存为一个revision（版次），这样就可以回滚到某个特定revision。
> 默认配置下，Kubernetes只会保留最近的几个revision，可以在Deployment配置文件中通过revisionHistoryLimit属性增加revision数量

**1.再次更新版本号为2.4.17，并带上参数`--record`部署应用**

**2.--record的作用是将当前命令记录到revision记录中，这样我们就可以知道每个revison对应的是哪个配置文件了。通过kubectl rollout history deployment httpd查看revison历史记录**

![image-20211226154144859](https://gitee.com/guoshaosong/chaungtu/raw/master/images/202112261541936.png)

![image-20211226154213879](https://gitee.com/guoshaosong/chaungtu/raw/master/images/202112261542935.png)

**3.进行版本回退**

```
kubectl rollout undo deployment http-deployment --to-revision=3
```

![image-20211226154326945](https://gitee.com/guoshaosong/chaungtu/raw/master/images/202112261543003.png)

![image-20211226154339071](https://gitee.com/guoshaosong/chaungtu/raw/master/images/202112261544381.png)

![image-20211226154422766](https://gitee.com/guoshaosong/chaungtu/raw/master/images/202112261544824.png)

### 健康检查

#### 默认健康检查

> Kubernetes默认的健康检查机制：每个容器启动时都会执行一个进程，此进程由Dockerfile的CMD或ENTRYPOINT指定。如果进程退出时返回码非零，则认为容器发生故障，Kubernetes就会根据restartPolicy重启容器。

```yaml
#模拟故障
apiVersion: v1
kind: Pod
metadata:
  name: healthcheck
  labels:
    test: healthcheck
spec:
  restartPolicy: OnFailure
  containers:
  - name: healthcheck
    image: busybox
    args:
    - /bin/sh
    - -c
    - sleep 10; exit 1
    
#####
容器进程返回值非零，Kubernetes则认为容器发生故障，需要重启。有不少情况是发生了故障，但进程并不会退出。比如访问Web服务器时显示500内部错误，可能是系统超载，也可能是资源死锁，此时httpd进程并没有异常退出，在这种情况下重启容器可能是最直接、最有效的解决方案，那我们如何利用Health Check机制来处理这类场景呢？
答案就是Liveness探测。
```

#### Liveness探测

> Liveness探测让用户可以自定义判断容器是否健康的条件。如果探测失败，Kubernetes就会重启容器。
>
> 启动进程首先创建文件/tmp/healthy，30秒后删除，在我们的设定中，如果/tmp/healthy文件存在，则认为容器处于正常状态，反之则发生故障。
> livenessProbe部分定义如何执行Liveness探测：
> （1）探测的方法是：通过cat命令检查/tmp/healthy文件是否存在。如果命令执行成功，返回值为零，Kubernetes则认为本次Liveness探测成功；如果命令返回值非零，本次Liveness探测失败。
> （2）initialDelaySeconds：10指定容器启动10之后开始执行Liveness探测，我们一般会根据应用启动的准备时间来设置。比如某个应用正常启动要花30秒，那么initialDelaySeconds的值就应该大于30。
> （3）periodSeconds：5指定每5秒执行一次Liveness探测。Kubernetes如果连续执行3次Liveness探测均失败，则会杀掉并重启容器。

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: liveness
  labels:
    test: liveness
spec:
  restartPolicy: OnFailure
  containers:
  - name: liveness
    image: busybox
    args:
    - /bin/sh
    - -c
    - touch /tmp/healthy; sleep 30; rm -rf /tmp/healthy; sleep 600
    livenessProbe:
      exec:
        command:
        - cat
        - /tmp/healthy
      initialDelaySeconds: 10
      periodSeconds: 5
```

![image-20211226163651372](https://gitee.com/guoshaosong/chaungtu/raw/master/images/202112261636454.png)

#### Readiness探测

> 用户通过Liveness探测可以告诉Kubernetes什么时候通过重启容器实现自愈；Readiness探测则是告诉Kubernetes什么时候可以将容器加入到Service负载均衡池中，对外提供服务。

​	

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: readiness
  labels:
    test: readiness
spec:
  restartPolicy: OnFailure
  containers:
  - name: readiness
    image: busybox
    args:
    - /bin/sh
    - -c
    - touch /tmp/healthy; sleep 30; rm -rf /tmp/healthy; sleep 600
    readinessProbe:
      exec:
        command:
        - cat
        - /tmp/healthy
      initialDelaySeconds: 10
      periodSeconds: 5
```



|                  | liveness                                     | readiness                                         |
| ---------------- | -------------------------------------------- | ------------------------------------------------- |
| 探测失败后的行为 | Liveness探测是重启容器                       | 将容器设置为不可用，不接收Service转发的请求       |
| 两者相互独立     | 用Liveness探测判断容器是否需要重启以实现自愈 | 用Readiness探测判断容器是否已经准备好对外提供服务 |

#### Health Check在Scale Up中的应用

> 对于多副本应用，当执行Scale Up操作时，新副本会作为backend被添加到Service的负载均衡中，与已有副本一起处理客户的请求。考虑到应用启动通常都需要一个准备阶段，比如加载缓存数据、连接数据库等，从容器启动到真正能够提供服务是需要一段时间的。我们可以通过Readiness探测判断容器是否就绪，避免将请求发送到还没有准备好的backend

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: web
spec:
  selector:
     matchLabels:
        run: web
  replicas: 3
  template:
    metadata:
      labels:
        run: web
    spec:
      containers:
      - name: web
        image: httpd
        ports:
        - containerPort: 8080
        readinessProbe:
          httpGet:
            scheme: HTTP
            path: /health-check
            port: 8080
          initialDelaySeconds: 10
          periodSeconds: 5
---
apiVersion: v1
kind: Service
metadata:
  name: web-svc
spec:
  selector:
    run: web
  ports:
  - protocol: TCP
    port: 8080
    targetPort: 80
```

> 重点关注readinessProbe部分。这里我们使用了不同于exec的另一种探测方法httpGet。Kubernetes对于该方法探测成功的判断条件是http请求的返回代码在200～400之间。
> schema指定协议，支持HTTP（默认值）和HTTPS。
> path指定访问路径。
> port指定端口。
> 上面配置的作用是：
> （1）容器启动10秒之后开始探测。
> （2）如果http://[container_ip]:8080/healthy返回代码不是200～400，表示容器没有就绪，不接收Service web-svc的请求。
> （3）每隔5秒探测一次。
> （4）直到返回代码为200～400，表明容器已经就绪，然后将其加入到web-svc的负载均衡中，开始处理客户请求。
> （5）探测会继续以5秒的间隔执行，如果连续发生3次失败，容器又会从负载均衡中移除，直到下次探测成功重新加入。

#### Health Check在滚动更新中的应用

> 因为新副本本身没有异常退出，默认的Health Check机制会认为容器。器已经就绪，进而会逐步用新副本替换现有副本，其结果就是：当所有旧副本都被替换后，整个应用将无法处理请求，无法对外提供服务。如果这是发生在重要的生产系统上，后果会非常严重。
> 如果正确配置了Health Check，新副本只有通过了Readiness探测才会被添加到Service；如果没有通过探测，现有副本不会被全部替换，业务仍然正常进行。

```yaml
[root@master update]# vim app.v1.yml

apiVersion: apps/v1
apiVersion: apps/v1
kind: Deployment
metadata:
  name: app
spec:
  selector:
     matchLabels:
        run: app
  replicas: 10
  template:
    metadata:
      labels:
        run: app
    spec:
      containers:
      - name: app
        image: busybox
        args:
        - /bin/sh
        - -c
        - sleep 10;touch /tmp/health-check;sleep 30000
        readinessProbe:
          exec:
            command:
            - cat
            - /tmp/health-check
          initialDelaySeconds: 10
          periodSeconds: 5
```

```yaml
[root@master update]# vim app.v2.yml 
apiVersion: apps/v1
kind: Deployment
metadata:
  name: app
spec:
  selector:
     matchLabels:
        run: app
  replicas: 10
  template:
    metadata:
      labels:
        run: app
    spec:
      containers:
      - name: app
        image: busybox
        args:
        - /bin/sh
        - -c
        - sleep 3000
        readinessProbe:
          exec:
            command:
            - cat
            - /tmp/health-check
          initialDelaySeconds: 10
          periodSeconds: 5
```

**kubectl apply -f app.v1.yml *--record**

> 1、从 Pod 的 AGE 栏可判断，最后 5 个 Pod 是新副本，目前处于 NOT READY 状态。
> 2、旧副本从最初 10 个减少到 8 个。
> 再来看 kubectl get deployment app 的输出：
> 1、READY  8/10 表示期望的状态是 10 个 READY 的副本,实际状态8个READY 的副本
> 2、UP-TO-DATE 5 表示当前已经完成更新的副本数：即 5 个新副本。就是app.v2.yml
> 3、AVAILABLE 8 表示当前处于 READY 状态的副本数：即 8个旧副本。
> 在我们的设定中，新副本始终都无法通过 Readiness 探测，所以这个状态会一直保持下去。
> 上面我们模拟了一个滚动更新失败的场景。不过幸运的是：Health Check 帮我们屏蔽了有缺陷的副本，同时保留了大部分旧副本，业务没有因更新失败受到影响。
>
> 为什么新创建的副本数是 5 个，同时只销毁了 2 个旧副本？
> 原因是：滚动更新通过参数 maxSurge 和 maxUnavailable 来控制副本替换的数量。
> **maxSurge**
> 此参数控制滚动更新过程中副本总数的超过 DESIRED 的上限。maxSurge 可以是具体的整数（比如 3），也可以是百分百，向上取整。maxSurge 默认值为 25%。
> 在上面的例子中，DESIRED 为 10，那么副本总数的最大值为：
> roundUp(10 + 10 * 25%) = 13
> 所以我们看到  UP-TO-DATE + AVAILABLE就是 13。 
> **maxUnavailable**
> 此参数控制滚动更新过程中，不可用的副本相占 DESIRED 的最大比例。 maxUnavailable 可以是具体的整数（比如 3），也可以是百分百，向下取整。maxUnavailable 默认值为 25%。
> 在上面的例子中，DESIRED 为 10，那么可用的副本数至少要为：
> 10 - roundDown(10 * 25%) = 8
> 所以我们看到 AVAILABLE 就是 8。
> maxSurge 值越大，初始创建的新副本数量就越多；maxUnavailable 值越大，初始销毁的旧副本数量就越多。

### 数据管理

> 我们经常会说：容器和Pod是短暂的。其含义是它们的生命周期可能很短，会被频繁地销毁和创建。容器销毁时，保存在容器内部文件系统中的数据都会被清除。
> 为了持久化保存容器的数据，可以使用Kubernetes Volume。
> Volume的生命周期独立于容器，Pod中的容器可能被销毁和重建，但Volume会被保留。
> 本质上，Kubernetes Volume是一个目录，这一点与Docker Volume类似。当Volume被mount到Pod，Pod中的所有容器都可以访问这个Volume。Kubernetes Volume也支持多种backend类型，包括emptyDir、hostPath、GCE Persistent Disk、AWS Elastic Block Store、NFS、Ceph等，完整列表可参考https://kubernetes.io/docs/concepts/storage/volumes/#types-of-volumes。

#### emptyDir

> emptyDir是最基础的Volume类型。正如其名字所示，一个emptyDir Volume是Host上的一个空目录。emptyDir Volume对于容器来说是持久的，对于Pod则不是。当Pod从节点删除时，Volume的内容也会被删除。但如果只是容器被销毁而Pod还在，则Volume不受影响。也就是说：emptyDir Volume的生命周期与Pod一致。
> Pod中的所有容器都可以共享Volume，它们可以指定各自的mount路径。

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: producer-consumer
spec:
  containers:
  - image: centos
    name: producer
    volumeMounts:
    - mountPath: /producer_dir
      name: shared-volume
    args:
    - /bin/sh
    - -c
    - echo "hello world" > /producer_dir/hello; sleep 30000

  - image: centos
    name: consumer
    volumeMounts:
    - mountPath: /consumer_dir
      name: shared-volume
    args:
    - /bin/sh
    - -c
    - cat /consumer_dir/hello; sleep 30000
    
  volumes:
  - name: shared-volume
    emptyDir: {}
```

#### hostPath

> hostPath Volume 的作用是将Docker Host 文件系统中已经存在的目录mount给Pod的容器。大部分应用不会使用hostPath Volume，因为这实际上增加了Pod与节点的耦合，限制了Pod的使用。不过那些需要访问Kubernetes或Docker内部数据（配置文件和二进制库）的应用则需要使用hostPath，如果Pod 被销毁了，hostPath对应的目录还是会被保留，不过一旦Host崩溃,hostPath 也就无法访问了。

```yaml
apiVersion: v1
kind: Pod
metadata:
name: producer-consumer
spec:
containers:
- image: centos
name: producer
volumeMounts:
- mountPath: /producer_dir
name: shared-volume
args:
- /bin/sh
- -c
- echo "hello world" > /producer_dir/hello; sleep 30000

volumes:
- name: shared-volume
hostPath:
path: /home/k8s/2019-08-30 #宿主机挂载点
```

#### 外部存储

> 如果Kubernetes 部署在云上，那么可以直接使用云硬盘作为Volume 
>
> Kubernetes Volume 也可以使用主流的分布式存储，比如Ceph、GlustetFS 等
>
> Ceph文件系统的/some/path/in/side/cephfs 目录被mount到容器路径/test/ceph
> 相当于emptyDir和hostPath，这些Volume类型的最大特点就是不依赖Kubernetes。Volume的底层基础设施由独立的存储系统管理，与Kubernetes集群是分离的。数据被持久化后，即是整个Kubernetes崩溃也不会受损。

#### PersistentVolume & PersistentVolumeClaim

> PersistentVolume（PV）是外部存储系统中的一块存储空间，由管理员创建和维护。与Volume一样，PV具有持久性，生命周期独立于Pod。Kubernetes支持多种类型的PersistentVolume，比如AWS EBS、Ceph、NFS等
> PersistentVolumeClaim（PVC）是对PV的申请（Claim）。PVC通常由普通用户创建和维护。需要为Pod分配存储资源时，用户可以创建一个PVC，指明存储资源的容量大小和访问模式（比如只读）等信息，Kubernetes会查找并提供满足条件的PV。

##### NFS PersistentVolume



### 常见命令

#### 获取node节点信息

```
kubectl get node
```

#### 查看集群信息

```
kubectl cluster-info
```

![image-20211224173649791](https://gitee.com/guoshaosong/chaungtu/raw/master/images/202112241736842.png)

#### 部署一个应用

```
kubectl run nginx --image=nginx --port=80
```

**说明**

> nginx-01 是应用的名称
>
> image 指定镜像
>
> port 端口映射

![image-20211224175050266](https://gitee.com/guoshaosong/chaungtu/raw/master/images/202112241750315.png)

#### 端口映射出來

```
kubectl expose deployment/nginx-02 --type="NodePort" --port 80
```

![image-20211224181306567](https://gitee.com/guoshaosong/chaungtu/raw/master/images/202112241813604.png)

被映射出来的端口是随机的

#### pod简介

Pod是容器的集合，通常会将紧密相关的一组容器放到一个Pod中，同一个Pod中的所有容器共享IP地址和Port空间，也就是说它们在一个network namespace中。并且pod是k8s调度的最小单位

获取pod信息

```
kubectl get pod
kubectl get pod --all-namespaces -o wide
```

![image-20211224175358747](https://gitee.com/guoshaosong/chaungtu/raw/master/images/202112241753788.png)

#### 查看应用被映射到了节点的那个端口

```
kubectl get services
```

![image-20211224175656884](https://gitee.com/guoshaosong/chaungtu/raw/master/images/202112241756920.png)

#### Scale应用

默认情况下应用只会运行一个副本，可以通过`kubectl get deployments`查看副本数

![image-20211224180452372](https://gitee.com/guoshaosong/chaungtu/raw/master/images/202112241804411.png)

**增加副本数**

```
kubectl scale deployments/nginx-02 --replicas=3
```

![image-20211224180616114](https://gitee.com/guoshaosong/chaungtu/raw/master/images/202112241806154.png)

![image-20211224180724009](https://gitee.com/guoshaosong/chaungtu/raw/master/images/202112241807045.png)

#### 滚动更新

```
kubectl set image deployments/nginx-02 nginx-02=ubuntu
```

![image-20211225110120230](https://gitee.com/guoshaosong/chaungtu/raw/master/images/202112251101284.png)![image-20211225110157906](https://gitee.com/guoshaosong/chaungtu/raw/master/images/202112251101964.png)

**可以看到正在滚动更新**

#### 版本回退

```
kubectl rollout undo deployments/nginx-02
```

#### 查看重复集

```
kubectl get replicaset
#查看详细信息时使用describe
```

![image-20211225143759564](https://gitee.com/guoshaosong/chaungtu/raw/master/images/202112251437620.png)

