算法的复杂度

复杂度分析

事后统计法

大O表示法

![<u></u>](https://gitee.com/guoshaosong/chuangtu/raw/master/images/202202281528326.png)

时间复杂度

![image-20220228161311660](https://gitee.com/guoshaosong/chuangtu/raw/master/images/202202281613803.png)

> 一般来说，复杂度高的算法比复杂度低的算法慢

空间复杂度

![image-20220305113145555](https://gitee.com/guoshaosong/chuangtu/raw/master/images/202203051131742.png)

![](https://gitee.com/guoshaosong/chuangtu/raw/master/images/202203051131430.png)



### 递归

> 避免无限递归，要有递归结束的条件

![image-20220228164726458](https://gitee.com/guoshaosong/chuangtu/raw/master/images/202202281647564.png)

![](https://gitee.com/guoshaosong/chuangtu/raw/master/images/202203051133849.png)

![image-20220305112840716](https://gitee.com/guoshaosong/chuangtu/raw/master/images/202203051128956.png)

### 斐波那契数列（爬楼梯）

> 力扣第70题

![image-20220228185956227](https://gitee.com/guoshaosong/chuangtu/raw/master/images/202202281859338.png)







### 汉诺塔问题

![image-20220304202138493](https://gitee.com/guoshaosong/chuangtu/raw/master/images/202203042021798.png)![image-20220304202334324](https://gitee.com/guoshaosong/chuangtu/raw/master/images/202203042023499.png)![image-20220304202440391](https://gitee.com/guoshaosong/chuangtu/raw/master/images/202203042024563.png)![image-20220304203542761](https://gitee.com/guoshaosong/chuangtu/raw/master/images/202203042035949.png)

```pyhton
def hannuota(n, first, second, third):
    if n > 0:
        hannuota(n - 1, first, third, second)
        print("moving from %s to %s" % (first, third))
        hannuota(n - 1, second, first, third)


if __name__ == '__main__':
    hannuota(3, 'first', 'second', 'third')
```

### 查找

![image-20220305203738607](https://gitee.com/guoshaosong/chuangtu/raw/master/images/202203052037855.png)

#### 顺序查找

![image-20220305204707804](https://gitee.com/guoshaosong/chuangtu/raw/master/images/202203052047957.png)

```python
def linear_search(li, val):
    for ind, v in enumerate(li):
        if v == val:
            return ind
    else:
        return None

"""
      enumerate() 函数用于将一个可遍历的数据对象(如列表、元组或字符串)组合为一个索引序列，同时列出数据和数据下标
      
"""
```

#### 二分查找

```python
def binary_search(li, val):
    left = 0
    right = len(li)
    li = sorted(li)
    while left <= right:  # 候选区还有值，这是二分查找进行循环的条件
        mid = (left + right) // 2
        if val == li[mid]:  # 查找的值就是中间值
            print('已经找到你要查找得值%s' % li[mid])
            return
        elif val < li[mid]:  # 查找的值比mid小
            right = mid - 1
        else:  # 查找的值比mid大
            left = mid + 1
    else:  # 没有找到
        return None
```

### 排序

![image-20220305215518860](https://gitee.com/guoshaosong/chuangtu/raw/master/images/202203052155028.png)

#### low b三人组（时间复杂度都是O（n2））

##### 冒泡排序

![image-20220305220213235](https://gitee.com/guoshaosong/chuangtu/raw/master/images/202203052202391.png)

````python
def bubble_sort(li):
    for i in range(len(li) - 1):  # 第多少趟
        state = False
        for j in range(len(li) - i - 1):
            if li[j] > li[j + 1]:
                li[j], li[j + 1] = li[j + 1], li[j]
                state = True
        print(li)
        if not state:
            return
````

##### 选择排序

![image-20220307162546862](https://gitee.com/guoshaosong/chuangtu/raw/master/images/202203071627503.png)

```python
def select_simple(li):
    li_new = []
    for i in range(len(li)):
        num = min(li)
        li_new.append(num)
        li.remove(num)
        print(li)
    return li_new


print(select_simple([3, 2, 4, 5, 6, 1, 8, 9]))

"""
使用了连个列表，增加了没有必要的内存消耗
"""
```

##### 插入排序

![image-20220308093952231](https://gitee.com/guoshaosong/chuangtu/raw/master/images/202203080939474.png)

![image-20220308095335325](https://gitee.com/guoshaosong/chuangtu/raw/master/images/202203080953452.png)			
